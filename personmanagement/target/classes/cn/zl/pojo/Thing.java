package cn.zl.pojo;

import java.util.Date;

public class Thing {
    private Integer id;

    private Date createtime;

    private String address;

    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Thing(Integer id, Date createtime, String address, String remarks) {
        this.id = id;
        this.createtime = createtime;
        this.address = address;
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Thing{" +
                "id=" + id +
                ", createtime=" + createtime +
                ", address='" + address + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }

    public Thing() {
        super();
    }
}