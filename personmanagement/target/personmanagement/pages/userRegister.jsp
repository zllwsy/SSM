<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%--
  Created by IntelliJ IDEA.
  User: mec
  Date: 2019/10/12
  Time: 14:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css" />

    <!--引入JQuery-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/bootstrap.js"></script>
</head>
<body>
<!--最外层的布局容器-->
<div class="container">
    <!--LOGO部分-->
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-6">
            <img src="../img/logo3.png" width="240px" height="100px"/>
        </div>
        <div class="col-md-4 hidden-sm hidden-xs">
            <img src=""/>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6" style="line-height: 100px;height: 100px;">

        </div>
    </div>




    <!-- 第三部分 -->
    <form action="${pageContext.request.contextPath}/user/userRegister.action" method="post">
    <div style="background: url(../img/background/back1.jpg);height: 550px;">
        <div style="position: absolute;top: 200px; left: 350px; border: 5px solid darkgray;width: 50%;height: 50%;background-color: white;">
            <table width="30%" height="45%" align="center">
            <div>
                <div colspan="2"  style="margin-left: 280px;margin-top: 20px">
                    <font color="blue" size="6">用户注册</font><br/>
                    USER REGISTER
                </div>

            </div><br/>

            <div style="margin-left: 280px">
                <div>用&nbsp;&nbsp;户&nbsp;名:<input type="text" name="username"/></div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:<input type="password" name="password"/></div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">确认密码:<input type="password" name="repassword"/></div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">电话号码:<input type="text" name="number"/></div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名:<input type="text" name="realname"/></div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别:<input type="radio" name="sex" /> 男
                          <input type="radio" name="sex" /> 女
                </div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px">地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址:
                    <input type="text" name="address"/>
                </div>
            </div>

            <div style="margin-left: 280px">
                <div style="margin-top: 5px;margin-left: 120px">
                <input type="submit" value="注册" />
                </div>
            </div>
            </table>
        </div>
    </div>
    </form>


    <!-- 第五部分 -->
    <div style="text-align: center;">
        <a href="#">关于我们</a>
        <a href="#">联系我们</a>
        <a href="#">招贤纳士</a>
        <a href="#">法律声明</a>
        <a href="#">友情链接</a>
        <a href="#">支付方式</a>
        <a href="#">配送方式</a>
        <a href="#">服务声明</a>
        <a href="#">广告声明</a>
        <br />
        Copyright © 2019-2020 Mr Zhang Lang. All rights reserved.
    </div>
</div>
</body>
</html>
