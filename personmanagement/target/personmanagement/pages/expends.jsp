<%--
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mec
  Date: 2019/10/13
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css"/>

    <!--引入JQuery-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/css.css">
</head>
<body>
<style type="text/css">
    .navbar {
        background: skyblue;
    }

    .collapse {

    }

    .navbar-collapse.navbar-nav > li > ul > li > a {
        color: black;
    }

    .dropdown {
        color: black
    }
    .left{
        float: left;
        top: 0;
        left: 0;
    }
    .div4{
        margin-left: 50px;
        border: 1px solid #2aabd2;
        float: right;
        top: 0;
        left: 0;
    }
    .connector-table-style{
        border: 1px solid #999;
        padding: 10px;
    }

    .contacts table tr td{
        font-size: 17px;
        border: solid 1px #999;
    }/*此处是指包含div4类选择器下的table标签下的tr下的td标签设置的属性和属性值，参考CSS各种选择器的写法*/
    .expends table tr td{
        font-size: 17px;
        border: solid 1px #999;
    }

</style>
<div class="container">
    <!--LOGO部分-->
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-6">
            <img src="${pageContext.request.contextPath}/img/logo3.png" width="240px" height="100px"/>
        </div>
        <div class="col-md-4 hidden-sm hidden-xs">
            <img src=""/>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6" style="line-height: 100px;height: 100px;">

        </div>
    </div>

    <!--导航栏部分-->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="hand.jsp">首页</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">个人信息管理<span
                                class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="${pageContext.request.contextPath}/user/findUser.action?id=${_USER_.id}"><span class="text-info">查看个人信息</span></a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/pages/update.jsp"><span class="text-info">修改个人信息</span></a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/user/logout.action"><span class="text-info">更换账号</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">联系人信息管理<span
                                class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="${pageContext.request.contextPath}/contact/showcontacts.action"><span class="text-info">查看联系人</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">修改联系人</span></a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/pages/insertContacts.jsp"><span class="text-info">添加联系人</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">账务管理<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="${pageContext.request.contextPath}/expend/showexpend.action"><span class="text-info">支出管理</span></a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/income/showincome.action"><span class="text-info">收入管理</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">辅助功能管理<span
                                class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="${pageContext.request.contextPath}/thing/showthing.action"><span class="text-info">备忘录</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">收藏夹</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">关于系统<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#"><span class="text-info">系统的版本</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">系统的提示</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">系统的更新</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">系统的退出</span></a>
                            </li>
                        </ul>
                    </li>
                    <br/>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">全部分类 <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="#"><span class="text-info">个人信息管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">联系人信息管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">账务管理</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#"><span class="text-info">日常日志管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">辅助功能管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">关于系统</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#"><span class="text-info">课程管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">课程表管理</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#"><span class="text-info">工作流程管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">每日行程管理</span></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#"><span class="text-info">人事管理</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="text-info">会议管理</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">搜索</button>
                </form>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <!--左边显示-->
    <div style="float: left">
        <div class="left">
            <table width="300" height="152" bgcolor="#000000">
                <%--            <img src="${pageContext.request.contextPath}/images/systemImages/fg_left01.jpg"><br/>--%>

                <%--            <%@include file="login.jsp"%>--%>
                <%@include file="../pages/login.jsp" %>
<%--                <img src="${pageContext.request.contextPath}/images/systemImages/fg_left02.fg_left202.jpg"><br/>--%>
<%--                <img src="${pageContext.request.contextPath}/images/systemImages/fg_left03.fg_left303.jpg"><br/>--%>
            </table>
        </div>
    </div>

    <!--信息栏-->
    <div class="expends" style="padding: 20px;margin-left: 200px">
        <table class="connector-table-style">
            <!--<div class="contacts">联系人信息列表</div>-->
            <caption class="expends">支出列表</caption>
            <div><a href="${pageContext.request.contextPath}/pages/insertexpend.jsp">添加新的支出</a></div>
            <thead>
            <tr>
                <td>ID</td>
                <td>收入</td>
                <td>日期</td>
                <td>备注</td>
                <td>操作</td>

            </tr>
            </thead>
            <tbody>
            <c:forEach items="${_LIST_}" var="e">
                <tr>
                    <td>${e.id}</td>
                    <td>${e.money}</td>
                    <td>${e.data}</td>
                    <td>${e.remarks}
                    <td>
                        <a href="${pageContext.request.contextPath}/expend/findexpend.action?id=${e.id}"><span style="font-size: 17px;color: red">修改</span></a>
                        <a href="${pageContext.request.contextPath}/expend/deleteexpend.action?id=${e.id}"><span style="font-size: 17px;color: red">删除</span></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>






<!-- 底部显示 -->
<div style="text-align: center;">
    <a href="#">关于我们</a>
    <a href="#">联系我们</a>
    <a href="#">招贤纳士</a>
    <a href="#">法律声明</a>
    <a href="#">友情链接</a>
    <a href="#">支付方式</a>
    <a href="#">配送方式</a>
    <a href="#">服务声明</a>
    <a href="#">广告声明</a>
    <br/>
    Copyright © 2019-2020 Mr Zhang Lang. All rights reserved.
</div>
</div>



</body>
</html>


