package cn.zl.pojo;

public class Contacts {
    private Integer id;

    private String contactname;

    private String number;

    private String email;

    private String qq;

    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname == null ? null : contactname.trim();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Contacts(Integer id, String contactname, String number, String email, String qq, String address) {
        this.id = id;
        this.contactname = contactname;
        this.number = number;
        this.email = email;
        this.qq = qq;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "id=" + id +
                ", contactname='" + contactname + '\'' +
                ", number='" + number + '\'' +
                ", email='" + email + '\'' +
                ", qq='" + qq + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public Contacts() {
        super();
    }
}