<%@ page import="cn.zl.utils.StringHelper" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<table width="300" height="152"
       background="${pageContext.request.contextPath}/images/systemImages/fg_left01.jpg"
       style="background-repeat: no-repeat" >
    <tr>
        <td>&nbsp;</td>
    </tr>

    <!-- el表达式，判定session对象中是否有用户_USER_
        数据表字段=实体类中属性-提交页面的name-展示页面el中的属性名
    -->
    <c:if test="${empty sessionScope._USER_}">
        <tr>
            <td width="100%">
                <form name="loginForm" method="post"
                      action="${pageContext.request.contextPath}/user/login.action"
                      method="post">
                    <!-- struts2,jsp,servlet,springmvc -->
                    <div>
                        用户：<input type="text" name="userName" id="userName"
                                  style="width:100px;"/></div>
                    <div>
                        密码：<input type="password" name="password"  id="password"
                                  style="width:100px;"/></div>
                    <br/>
                    <div >
                        <input type="image" class="input1" width="51" height="20"
                               src="${pageContext.request.contextPath}/images/systemImages/fg_login.jpg">

                        <img id="ajax_register2" height="19" width="49" border=0
                             src="${pageContext.request.contextPath}/images/systemImages/fg_reg.jpg" />

                        <a href="#" >
                            <IMG height="19" width="49" border=0
                                 src="${pageContext.request.contextPath}/images/systemImages/fg_pswfind.jpg" ></IMG>
                        </a>
                    </div>
                </form>
            </td>
        </tr>
    </c:if>

    <c:if test="${not empty _USER_}">
        <tr>
            <td>
                <table width="200" border="0" align="center">
                    <tr>
                        <td width="200" height="25">
                                ${sessionScope._USER_.userName }登录成功
                        </td>
                    </tr>
                    <tr>
                        <td height="25"><%=StringHelper.currentlyTime()%></td>
                    </tr>
                    <tr>
                        <td height="25">
                            用户姓名：${sessionScope._USER_.realName}</td>
                    </tr>
                    <tr>
                        <td height="20" align="right" valign="middle">
                            <a href="${pageContext.request.contextPath}/user/logout.action">安全退出</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </c:if>
</table>