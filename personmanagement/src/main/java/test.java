import cn.zl.mapper.UserMapper;
import cn.zl.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author zhanglang
 * @Date 2019/10/12
 */
public class test {
    @Test
    public void testSpringMybatis() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        System.out.println(ac.getBean("dataSource"));
        UserMapper dao = (UserMapper) ac.getBean("userMapper");
        User user = dao.selectByPrimaryKey(3);
        System.out.println(user);
    }
}
