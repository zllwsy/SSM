package cn.zl.bll;

import cn.zl.pojo.Income;
import cn.zl.pojo.IncomeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
public interface IncomeBll {
    int countByExample(IncomeExample example);

    int deleteByExample(IncomeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Income record);

    int insertSelective(Income record);

    List<Income> selectByExample(IncomeExample example);

    Income selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Income record, @Param("example") IncomeExample example);

    int updateByExample(@Param("record") Income record, @Param("example") IncomeExample example);

    int updateByPrimaryKeySelective(Income record);

    int updateByPrimaryKey(Income record);
}
