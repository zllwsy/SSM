package cn.zl.bll;

import cn.zl.mapper.ContactsMapper;
import cn.zl.pojo.Contacts;
import cn.zl.pojo.ContactsExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Service("contactsBll")
public class ContactsBllImpl implements ContactsBll{
    @Resource(name = "contactsMapper")
    private ContactsMapper contactsMapper;
    public List<Contacts> selectByExample(ContactsExample example) {
        return contactsMapper.selectByExample(example);
    }

    public int insert(Contacts record) {
        return contactsMapper.insert(record);
    }

    public int deleteByPrimaryKey(Integer id) {
        return contactsMapper.deleteByPrimaryKey(id);
    }

    public Contacts selectByPrimaryKey(Integer id) {
        return contactsMapper.selectByPrimaryKey(id);
    }

    public int updateByPrimaryKey(Contacts record) {
        return contactsMapper.updateByPrimaryKey(record);
    }
}
