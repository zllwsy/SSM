package cn.zl.bll;

import cn.zl.mapper.IncomeMapper;
import cn.zl.pojo.Income;
import cn.zl.pojo.IncomeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Service("incomeBll")
public class IncomeBllImpl implements IncomeBll {
    @Autowired
    private IncomeMapper  incomeMapper;
    public int countByExample(IncomeExample example) {
        return 0;
    }

    public int deleteByExample(IncomeExample example) {
        return 0;
    }

    public int deleteByPrimaryKey(Integer id) {
        return incomeMapper.deleteByPrimaryKey(id);
    }

    public int insert(Income record) {
        return incomeMapper.insert(record);
    }

    public int insertSelective(Income record) {
        return 0;
    }

    public List<Income> selectByExample(IncomeExample example) {
        return incomeMapper.selectByExample(example);
    }

    public Income selectByPrimaryKey(Integer id) {
        return incomeMapper.selectByPrimaryKey(id);
    }

    public int updateByExampleSelective(Income record, IncomeExample example) {
        return 0;
    }

    public int updateByExample(Income record, IncomeExample example) {
        return 0;
    }

    public int updateByPrimaryKeySelective(Income record) {
        return 0;
    }

    public int updateByPrimaryKey(Income record) {
        return incomeMapper.updateByPrimaryKey(record);
    }
}
