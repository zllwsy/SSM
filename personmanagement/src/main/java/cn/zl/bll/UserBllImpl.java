package cn.zl.bll;


import cn.zl.mapper.UserMapper;
import cn.zl.pojo.User;
import cn.zl.pojo.UserExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/11
 */
@Service("UserBll")
public class UserBllImpl implements UserBll{
    @Resource(name="userMapper")

    private UserMapper userMapper;
    public List<User> selectByExample(UserExample example)
    {
        return userMapper.selectByExample(example);
    }

    public int insert(User record) {
        return userMapper.insert(record);
    }

    public int updateByPrimaryKey(User record) {
        return userMapper.updateByPrimaryKey(record);
    }

    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }
}
