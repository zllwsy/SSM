package cn.zl.bll;

import cn.zl.pojo.Contacts;
import cn.zl.pojo.ContactsExample;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
public interface ContactsBll {
    List<Contacts> selectByExample(ContactsExample example);//查看联系人信息

    int insert(Contacts record);

    int deleteByPrimaryKey(Integer id);

    Contacts selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(Contacts record);
}
