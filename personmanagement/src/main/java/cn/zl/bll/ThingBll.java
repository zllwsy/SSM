package cn.zl.bll;

import cn.zl.pojo.Contacts;
import cn.zl.pojo.Thing;
import cn.zl.pojo.ThingExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/11/20
 */
public interface ThingBll {
    int countByExample(ThingExample example);

    int deleteByExample(ThingExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Thing record);

    int insertSelective(Thing record);

    List<Thing> selectByExampleWithBLOBs(ThingExample example);

    List<Thing> selectByExample(ThingExample example);

    Thing selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Thing record, @Param("example") ThingExample example);

    int updateByExampleWithBLOBs(@Param("record") Thing record, @Param("example") ThingExample example);

    int updateByExample(@Param("record") Thing record, @Param("example") ThingExample example);

    int updateByPrimaryKeySelective(Thing record);

    int updateByPrimaryKeyWithBLOBs(Thing record);

    int updateByPrimaryKey(Thing record);
}
