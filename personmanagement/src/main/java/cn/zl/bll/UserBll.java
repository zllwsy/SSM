package cn.zl.bll;

/*@author zhanglang
  @Date 2019/10/11*/

import cn.zl.pojo.User;
import cn.zl.pojo.UserExample;

import java.util.List;

public interface UserBll {

    List<User> selectByExample(UserExample example);//用户登录登出

    int insert(User record);//用户注册

    int updateByPrimaryKey(User record);//用户修改

    User selectByPrimaryKey(Integer id);//用户个人信息
}
