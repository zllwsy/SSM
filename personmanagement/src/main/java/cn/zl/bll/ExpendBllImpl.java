package cn.zl.bll;

import cn.zl.mapper.ExpendMapper;
import cn.zl.pojo.Expend;
import cn.zl.pojo.ExpendExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Service("expendBll")
public class ExpendBllImpl implements ExpendBll {
    @Autowired
    private ExpendMapper expendMapper;
    public int countByExample(ExpendExample example) {
        return 0;
    }

    public int deleteByExample(ExpendExample example) {
        return 0;
    }

    public int deleteByPrimaryKey(Integer id) {
        return expendMapper.deleteByPrimaryKey(id);
    }

    public int insert(Expend record) {
        return expendMapper.insert(record);
    }

    public int insertSelective(Expend record) {
        return 0;
    }

    public List<Expend> selectByExample(ExpendExample example) {
        return expendMapper.selectByExample(example);
    }

    public Expend selectByPrimaryKey(Integer id) {
        return expendMapper.selectByPrimaryKey(id);
    }

    public int updateByExampleSelective(Expend record, ExpendExample example) {
        return 0;
    }

    public int updateByExample(Expend record, ExpendExample example) {
        return 0;
    }

    public int updateByPrimaryKeySelective(Expend record) {
        return 0;
    }

    public int updateByPrimaryKey(Expend record) {
        return expendMapper.updateByPrimaryKey(record);
    }
}
