package cn.zl.bll;

import cn.zl.mapper.ThingMapper;
import cn.zl.pojo.Contacts;
import cn.zl.pojo.Thing;
import cn.zl.pojo.ThingExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/11/20
 */
@Service("ThingBll")
public class ThingBllImpl implements ThingBll{
    @Resource(name = "thingMapper")
    private ThingMapper thingMapper;

    public int countByExample(ThingExample example) {
        return 0;
    }

    public int deleteByExample(ThingExample example) {
        return 0;
    }

    public int deleteByPrimaryKey(Integer id) {
        return thingMapper.deleteByPrimaryKey(id);
    }



    public int insert(Thing record) {
        return thingMapper.insert(record);
    }

    public int insertSelective(Thing record) {
        return 0;
    }

    public List<Thing> selectByExampleWithBLOBs(ThingExample example) {
        return null;
    }

    public List<Thing> selectByExample(ThingExample example) {
        return thingMapper.selectByExample(example);
    }

    public Thing selectByPrimaryKey(Integer id) {
        return thingMapper.selectByPrimaryKey(id);
    }

    public int updateByExampleSelective(Thing record, ThingExample example) {
        return 0;
    }

    public int updateByExampleWithBLOBs(Thing record, ThingExample example) {
        return 0;
    }

    public int updateByExample(Thing record, ThingExample example) {
        return 0;
    }

    public int updateByPrimaryKeySelective(Thing record) {
        return 0;
    }

    public int updateByPrimaryKeyWithBLOBs(Thing record) {
        return 0;
    }

    public int updateByPrimaryKey(Thing record) {
        return thingMapper.updateByPrimaryKey(record);
    }
}
