package cn.zl.utils;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;



public class StringHelper {
	public static Random random = new Random();

	public static String getStringDate(){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}
	
	public static String getStringTime(){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
		return sdf.format(date);
	}

	public static boolean validateString(String s){
		if(s != null && s.trim().length() > 0){
			return true;
		}
		return false;
	}

	public static boolean validateFloat(Float f){
		try {
			if(f != null && f > 0){
				return true;
			}
		} catch (Exception e) {}
		return false;
	}

	public static String createId(){
		StringBuffer sb = new StringBuffer();
		sb.append(getStringTime());
		sb.append("_");
		for (int i = 0; i < 2; i++) {
			sb.append(random.nextInt(9));
		}
		return sb.toString();
	}
	
	public static String currentlyTime() {
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL);
		return dateFormat.format(date);
	}
	@Test
	public void testDate(){
		System.out.println(StringHelper.getStringDate());
	}
}
