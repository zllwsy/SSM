package cn.zl.pojo;

public class User {
    private Integer id;

    private String username;

    private String password;

    private String number;

    private String sex;

    private String address;

    private String realname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public User(Integer id, String username, String password, String number, String sex, String address, String realname) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.number = number;
        this.sex = sex;
        this.address = address;
        this.realname = realname;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", number='" + number + '\'' +
                ", sex='" + sex + '\'' +
                ", address='" + address + '\'' +
                ", realname='" + realname + '\'' +
                '}';
    }

    public User() {
        super();
    }
}