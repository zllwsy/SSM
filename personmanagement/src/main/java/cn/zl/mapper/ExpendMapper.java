package cn.zl.mapper;

import cn.zl.pojo.Expend;
import cn.zl.pojo.ExpendExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExpendMapper {
    int countByExample(ExpendExample example);

    int deleteByExample(ExpendExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Expend record);

    int insertSelective(Expend record);

    List<Expend> selectByExample(ExpendExample example);

    Expend selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Expend record, @Param("example") ExpendExample example);

    int updateByExample(@Param("record") Expend record, @Param("example") ExpendExample example);

    int updateByPrimaryKeySelective(Expend record);

    int updateByPrimaryKey(Expend record);
}