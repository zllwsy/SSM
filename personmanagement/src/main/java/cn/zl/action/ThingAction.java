package cn.zl.action;


import cn.zl.bll.ThingBll;
import cn.zl.pojo.Contacts;
import cn.zl.pojo.Thing;
import cn.zl.pojo.ThingExample;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/11/20
 */
@Controller
@RequestMapping("/thing")
public class ThingAction {
    @Resource(name = "ThingBll")
    private ThingBll thingBll;
    @RequestMapping("/showthing")
    public String showthing(Model model){
        ThingExample example=new ThingExample();
        List<Thing> thing=thingBll.selectByExample(example);
        model.addAttribute("_LIST_",thing);
        return "thing";
    }
    @RequestMapping("/insertthing")
    public String insertthing(Model model, Thing record ) {
        int th=thingBll.insert(record);
        model.addAttribute("_LIST_", th);
        return "hand";
    }
    @RequestMapping("/deletething")
    public String deletecontact(int id,Model model){
        int th=thingBll.deleteByPrimaryKey(id);
        model.addAttribute("_LIST_",th);
        return "hand";
    }
    @RequestMapping("/findthing")
    public String findcontact(int id,Model model){
        Thing th=thingBll.selectByPrimaryKey(id);
        model.addAttribute("_LIST_",th);
        return "thingPre";
    }
    @RequestMapping("/update")
    public String update(Model model, Thing record) {
        int th= thingBll.updateByPrimaryKey(record);
        model.addAttribute(th);
        return "hand";
    }
}


