package cn.zl.action;


import cn.zl.bll.ContactsBll;
import cn.zl.pojo.Contacts;
import cn.zl.pojo.ContactsExample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Controller
@RequestMapping("/contact")
public class ContactsAction {
    @Resource(name = "contactsBll")
    private ContactsBll contactsBll;
    @RequestMapping("/showcontacts")
    public String showcontacts(Model model){
        ContactsExample example=new ContactsExample();
        List<Contacts> contacts=contactsBll.selectByExample(example);
        model.addAttribute("_LIST_", contacts);
        return "contacts";
    }
    @RequestMapping("/insertcontact")
    public String insertcontact(Model model, Contacts record ) {
        int con=contactsBll.insert(record);
        model.addAttribute("_LIST_", con);
        return "hand";
    }
    @RequestMapping("/deletecontact")
    public String deletecontact(int id,Model model){
        int contacts=contactsBll.deleteByPrimaryKey(id);
        model.addAttribute("_LIST_",contacts);
        return "hand";
    }
    @RequestMapping("/findcontact")
    public String findcontact(int id,Model model){
        Contacts contactss=contactsBll.selectByPrimaryKey(id);
        model.addAttribute("_LIST_",contactss);
        return "contactsPre";
    }
    @RequestMapping("/update")
    public String update(Model model,Contacts record) {
        int contacts= contactsBll.updateByPrimaryKey(record);
        model.addAttribute(contacts);
        return "hand";
    }
}
