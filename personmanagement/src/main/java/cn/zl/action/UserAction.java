package cn.zl.action;


import cn.zl.bll.UserBll;
import cn.zl.pojo.User;
import cn.zl.pojo.UserExample;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/*@author zhanglang
  @Date 2019/10/11*/
@Controller
@RequestMapping("/user")
public class UserAction {
    //调用业务层
    @Resource(name = "UserBll")
    private UserBll userBll;

    @RequestMapping("/login")
    public String login(String userName, String password, HttpSession session) {
        UserExample example = new UserExample();
        example.createCriteria().andUsernameEqualTo(userName).andPasswordEqualTo(password);
        List<User> users = userBll.selectByExample(example);
        session.setAttribute("_USER_", users.get(0));
        return "hand";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        //model.addAttribute("_USER_", null);
        session.removeAttribute("_USER_");
        return "hand";
    }

    @RequestMapping("/userRegister")
    public String userRegister(Model model, User record ) {
        int user=userBll.insert(record);
        model.addAttribute("_USER_", user);
        return "warning";
    }

    @RequestMapping("/findUser")
    public String findUser(int id,Model model){
        User user=userBll.selectByPrimaryKey(id);
        model.addAttribute("USER",user);
        return "person";
    }

    @RequestMapping("/update")
    public String update(Model model,User record) {
        int User=userBll.updateByPrimaryKey(record);
        model.addAttribute("_USER_",User);
        return "warning2";
    }
}
