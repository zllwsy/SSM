package cn.zl.action;

import cn.zl.bll.IncomeBll;

import cn.zl.pojo.Income;
import cn.zl.pojo.IncomeExample;

import cn.zl.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Controller
@RequestMapping("income")
public class IncomeAction {
    @Autowired
    private IncomeBll incomeBll;
    @RequestMapping("showincome")
    public String showincome(Model model){
        IncomeExample example=new IncomeExample();
        List<Income> incomes=incomeBll.selectByExample(example);
        model.addAttribute("_LIST_", incomes);
        return "incomes";

    }
    @RequestMapping("/insertincome")
    public String insertincome(Model model, Income record ) {
        int income=incomeBll.insert(record);
        model.addAttribute("_LIST_", income);
        return "hand";
    }
    @RequestMapping("/deleteincome")
    public String deleteincome(int id,Model model){
        int incomes=incomeBll.deleteByPrimaryKey(id);
        model.addAttribute("_LIST_",incomes);
        return "hand";
    }
    @RequestMapping("/findincome")
    public String findincome(int id,Model model){
        Income incomes=incomeBll.selectByPrimaryKey(id);
        model.addAttribute("_LIST_",incomes);
        return "incomePre";
    }
    @RequestMapping("/update")
    public String update(Model model,Income record) {
        int Income= incomeBll.updateByPrimaryKey(record);
        model.addAttribute(Income);
        return "hand";
    }
}
