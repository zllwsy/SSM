package cn.zl.action;

import cn.zl.bll.ExpendBll;

import cn.zl.pojo.Expend;
import cn.zl.pojo.ExpendExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author zhanglang
 * @Date 2019/10/13
 */
@Controller
@RequestMapping("expend")
public class ExpendAction {
    @Autowired
    private ExpendBll expendBll;
    @RequestMapping("showexpend")
    public String howexpend(Model model){
        ExpendExample example=new ExpendExample();
        List<Expend> expends=expendBll.selectByExample(example);
        model.addAttribute("_LIST_", expends);
        return "expends";

    }
    @RequestMapping("/insertexpend")
    public String insertexpend(Model model, Expend record ) {
        int expend=expendBll.insert(record);
        model.addAttribute("_LIST_", expend);
        return "hand";
    }
    @RequestMapping("/deleteexpend")
    public String deleteexpend(int id,Model model){
        int expends=expendBll.deleteByPrimaryKey(id);
        model.addAttribute("_LIST_",expends);
        return "hand";
    }
    @RequestMapping("/findexpend")
    public String findexpend(int id,Model model){
        Expend expends=expendBll.selectByPrimaryKey(id);
        model.addAttribute("_LIST_",expends);
        return "expendPre";
    }
    @RequestMapping("/update")
    public String update(Model model,Expend record) {
        int expend= expendBll.updateByPrimaryKey(record);
        model.addAttribute(expend);
        return "hand";
    }
}
