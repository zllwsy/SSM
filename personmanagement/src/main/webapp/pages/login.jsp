<%@ page language="java"
         import="java.util.*,cn.zl.pojo.User,
	     cn.zl.utils.StringHelper" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%--
  Created by IntelliJ IDEA.
  User: mec
  Date: 2019/10/11
  Time: 14:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<script type="text/javascript">
    function quit(){

    }
</script>
<body>

<table width="300" height="152"
       background="${pageContext.request.contextPath}/images/systemImages/fg_left01.jpg"
       style="background-repeat: no-repeat" >
    <tr>
        <td></td>
    </tr>

    <c:if test="${empty sessionScope._USER_}">
        <tr>
            <td width="100%">
                <form name="loginForm" method="post"
                      action="${pageContext.request.contextPath}/user/login.action"
                      method="post">
                    <div style="margin-left: 50px; margin-top: 45px">
                        用户：<input type="text" name="userName" id="userName"
                                  style="width:130px;"/></div>
                    <div style="margin-left: 50px;">
                        密码：<input type="password" name="password"  id="password"
                                  style="width:130px;"/></div>
                    <div style="margin-left: 110px; margin-top: 5px">
                        <input type="submit" value="登录">
<%--                        <button type="button">登录</button>--%>
<%--                        <input type="image" class="input1" width="51" height="20" border=0--%>
<%--                               src="../images/systemImages/fg_login.jpg">--%>
                        <a href="${pageContext.request.contextPath}/pages/userRegister.jsp">
                        <button type="button" >注册</button></a>
<%--                            <input type="image" class="input1" width="51" height="20" border=0--%>
<%--                               src="../images/systemImages/fg_reg.jpg" ></a>--%>
                    </div>
                </form>
            </td>
        </tr>
    </c:if>

    <c:if test="${not empty _USER_}">
        <tr>
            <td>
                <table>
                    <div style="margin-left: 70px">
                        <div width="200" height="5">
                                ${sessionScope._USER_.username }登录成功
                        </div>
                    </div>
                    <div style="margin-left: 70px">
                        <div height="5"><%=StringHelper.currentlyTime()%>
                        </div>
                    </div>
                    <div style="margin-left: 70px">
                        <div height="5">
                            用户姓名：${sessionScope._USER_.realname}
                        </div>
                    </div>
                    <div style="margin-left: 70px">
                        <div height="5">
                            <a href="${pageContext.request.contextPath}/user/logout.action">安全退出</a>
                        </div>
                    </div>
                </table>
            </td>
        </tr>
    </c:if>



</table>
</body>
</html>
